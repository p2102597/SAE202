/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15;

import java.util.ArrayList;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import static packMesClassesEtInterfaces.SAE202_Algos.compte_nb_conflits;

/**
 *
 * @author Maxen
 */



public class RecuitSimule {
    
    
    
    private static void graphDupl(Graph G,String s){
        for (int i = 0; i < G.getNodeCount(); i++) {
            
            G.getNode(i).addAttribute(s,(int) G.getNode(i).getAttribute("couleur"));
        }
    }
    
    private void graphFusion(Graph G,String src,String dest){
        for (int i = 0; i < G.getNodeCount(); i++) {
            G.getNode(i).setAttribute(dest, (int) G.getNode(i).getAttribute(src));
        }
    }
    
    public int run(Graph S, int kmax){
        AlgoDSAT.run(S, kmax);
        
        graphDupl(S,"couleurSmin");
        graphDupl(S,"CouleurVoisinMin");
        
        int i=0;int t=0;
        int imax=50;
        do {
            
            i++;
            if (i==5) {
                t++;
            }

            ListeVoisin(S, kmax);
            
            
            //Graph T=liste.get((int) (Math.random()*(liste.size()-1)));
            
//            System.out.println(compte_nb_conflits(S, "couleur")+" "+compte_nb_conflits(S, "couleurT"));

            
            if ((int)compte_nb_conflits(S, "CouleurVoisinMin")<(int)compte_nb_conflits(S, "couleur")) {
                graphFusion(S,"CouleurVoisinMin","couleur");
                
            }
            else{
                double H=Math.random();
                //System.out.println(H+" "+Math.exp((compte_nb_conflits(S, "couleur")-compte_nb_conflits(S, "couleurT"))/risque(t)));
                
                if (H<Math.exp((compte_nb_conflits(S, "couleur")-compte_nb_conflits(S, "CouleurVoisinMin"))/risque(t))) {
                    
                    graphFusion(S,"CouleurVoisinMin","couleur");
                }
            }
            if (compte_nb_conflits(S, "couleur")<compte_nb_conflits(S, "couleurSmin")) {
                graphFusion(S,"couleur","couleurSmin");
                i=0;
            }
            
            
            
            
        } while (i<imax);
        
        return compte_nb_conflits(S, "couleurSmin");
    }
    
    private double risque(double t){
        return 1; //Math.pow(10, t)*10;
    }
    
    private Graph min(ArrayList<Graph> LG){
        Graph graphmin=LG.get(0);
        int min =compte_nb_conflits(graphmin, "couleur");
        for (int i = 1; i < LG.size(); i++) {
            if (min>compte_nb_conflits(LG.get(i), "couleur")) {
                min=compte_nb_conflits(LG.get(i), "couleur");
                graphmin=LG.get(i);
            }
        }
        return graphmin;
    }
    
    private void ListeVoisin(Graph S, int kmax){
        ArrayList<Integer> Point =new ArrayList();
        for (Edge e: S.getEachEdge()) {
            if (e.getNode0().getAttribute("couleur")==e.getNode1().getAttribute("couleur")) {
                if (!Point.contains(e.getNode0().getIndex())) {
                    Point.add(e.getNode0().getIndex());
                }
                if (!Point.contains(e.getNode1().getIndex())) {
                    Point.add(e.getNode1().getIndex());
                }
            }
        }
        
        for (int j = 0; j < Point.size(); j++) {
            
            for (int i = 0; i < kmax; i++) {
                if (i!= (int) S.getNode(j).getAttribute("couleur")) {
                    S.getNode(j).setAttribute("couleur", i);
                    
                    
                    if (compte_nb_conflits(S, "couleur")<=compte_nb_conflits(S, "CouleurVoisinMin")) {
                        graphFusion(S, "couleur", "CouleurVoisinMin");
                        //System.out.println(compte_nb_conflits(S, "couleur")+" "+compte_nb_conflits(S, "CouleurVoisinMin"));
                    }
//                    System.out.println(compte_nb_conflits(S, "couleur"));
                }
                
            }
            
        }
        
        //System.out.println(voisin.size());
    }
    
    
}
