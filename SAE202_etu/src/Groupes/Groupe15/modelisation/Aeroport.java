package Groupes.Groupe15.modelisation;

import java.awt.Point;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


/**
 *
 * @author Maxen
 */
public class Aeroport {
    private String id;
    private String name;
    private double x,y;
    
    private double convertDegMinSecToDegresDecimaux (int deg, int min, int sec, String Oriantation){
        double coef;
        if (Oriantation.equals("N") || Oriantation.equals("E")) {
            coef=1.0;
        }
        else{
            coef=-1.0;
        }
        return coef*(deg+min/60.0+sec/3600.0);
    }
    /**
     * converti des deg,min,sec en x et y
     * @param degLat degre lattitude
     * @param minLat minute lattitude
     * @param secLat seconde lattitude
     * @param OriantationLat Oriantation lattitude
     * @param degLong degre longitude
     * @param minLong minute longitude
     * @param secLong seconde longitude
     * @param OriantationLong  Oriantation longitude
     */
    public void convert(int degLat, int minLat, int secLat, String OriantationLat,int degLong, int minLong, int secLong, String OriantationLong){
        double latitude=convertDegMinSecToDegresDecimaux (degLat, minLat, secLat, OriantationLat);
        double longitude=convertDegMinSecToDegresDecimaux (degLong, minLong, secLong, OriantationLong);
        latitude=latitude*(Math.PI/180.0);
        longitude=longitude*(Math.PI/180.0);
        
        
        x= (6371*Math.cos(latitude)*Math.sin(longitude));
        y= (6371*Math.cos(latitude)*Math.cos(longitude));
    }

    public Aeroport(String id, String name, int degLat, int minLat, int secLat, String OriantationLat,int degLong, int minLong, int secLong, String OriantationLong) {
        this.id = id;
        this.name = name;
        convert(degLat, minLat, secLat, OriantationLat,degLong, minLong, secLong, OriantationLong);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            Aeroport tmp = (Aeroport) obj;
            if (tmp.getId()==this.id) {
                return true;
            }
        }
        return false;
    }
    
    
    
    @Override
    public String toString() {
        return "Aeroport{" + "id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + '}';
    }
    
    
    
    
    
    
}
