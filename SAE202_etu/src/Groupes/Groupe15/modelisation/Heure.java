/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15.modelisation;

/**
 *
 * @author Maxen
 */
public class Heure {
    int h;
    int m;
    
    
    public Heure(int h, int m) {
        this.h = h;
        this.m = m;
    }

    public int getH() {
        return h;
    }

    public int getM() {
        return m;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setM(int m) {
        this.m = m;
    }
    public void setM(int h,int m) {
        this.h = h;
        this.m = m;
    }
    
    /**
     * ajoute a l'heure un nombre en minute
     * @param m int minute a ajouter
     */
    public void addmin(int m){
        this.m+=m;
        if (this.m>=60) {
            this.m=this.m%60;
            this.h++;
            if (h>=24) {
                this.h=0;
            }
        }
    }
    /**
     * 
     * @return en minite l'heure
     */
    public int retourneEnMinute(){
        return h*60+m;
    }

    
    @Override
    public boolean equals(Object obj) {
        Heure tmp = (Heure) obj;
        if (this == obj) {
            if (this.m==tmp.m&&this.h==tmp.h) {
                return true;
            }
        }
       return false;
    }
    
    
    
    @Override
    public String toString() {
        return "Heure{" + "h=" + h + ", m=" + m + '}';
    }
    
    
}
