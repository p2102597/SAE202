/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15.modelisation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Maxen
 */
public class FranceAeroport {
    private ArrayList<Aeroport> aeropoertListe = new ArrayList<>();
    /**
     * inisialise tous les aeroport de france de sa liste
     * @throws FileNotFoundException 
     */
    public FranceAeroport() throws FileNotFoundException {
        Scanner obj = new Scanner(new File("Donnees\\Test-model\\aeroports.csv"));
        
        while (obj.hasNextLine()) {
            String id=""; String name=""; int degLat=0; int minLat=0; int secLat=0; String OriantationLat=""; int degLong=0; int minLong=0; int secLong=0; String OriantationLong="";
            String Ligne= obj.nextLine();
            String tmp="";
            int y=0;
            for (int i = 0; i < Ligne.length(); i++) {
                if (Ligne.charAt(i)==';') {
                    if (y==0) {
                        id=tmp;
                    }
                    else if(y==1){
                        name=tmp;
                    }
                    else if(y==2){
                        degLat=Integer.parseInt(tmp);
                    }
                    else if(y==3){
                        minLat=Integer.parseInt(tmp);
                    }
                    else if(y==4){
                        secLat=Integer.parseInt(tmp);
                    }
                    else if(y==5){
                        OriantationLat=tmp;
                    }
                    else if(y==6){
                        degLong=Integer.parseInt(tmp);
                    }
                    else if(y==7){
                        minLong=Integer.parseInt(tmp);
                    }
                    else if(y==8){
                        secLong=Integer.parseInt(tmp);
                    }
                    tmp="";
                    i++;
                    y++;
                }
                tmp+=Ligne.charAt(i);
                if (i==Ligne.length()-1) {
                    OriantationLong=tmp;
                }
            }
            //System.out.println(id+" "+ name+" "+ degLat+" "+ minLat+" "+ secLat+" "+ OriantationLat+" "+ degLong+" "+ minLong+" "+ secLong+" "+ OriantationLong);
            aeropoertListe.add(new Aeroport(id, name, degLat, minLat, secLat, OriantationLat, degLong, minLong, secLong, OriantationLong));
        }
        obj.close();
    }

    public ArrayList<Aeroport> getAeropoertListe() {
        return aeropoertListe;
    }
    
    
    @Override
    public String toString() {
        String tmp="";
        for (Aeroport a:aeropoertListe) {
            tmp+=a;
            tmp+='\n';
        }
        return tmp;
    }
    
}
