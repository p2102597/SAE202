/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15.modelisation;

import java.util.ArrayList;

/**
 *
 * @author Maxen
 */
public class Vol {
    private String id;
    private String idAeroportDep;
    private String idAeroportArr;
    private int heureDep;
    private int minDep;
    private int dureeVol;
    
    /**
     * constructeur
     * @param id String id du vol
     * @param idAeroportDep String id de l'aeroport de depart
     * @param idAeroportArr String id de l'aeroport d'arrivee
     * @param heureDep int l'heur de depart
     * @param minDep int minute de depart
     * @param dureeVol int duree du vol en minute
     */
    public Vol(String id, String idAeroportDep, String idAeroportArr, int heureDep, int minDep, int dureeVol) {
        this.id = id;
        this.idAeroportDep = idAeroportDep;
        this.idAeroportArr = idAeroportArr;
        this.heureDep = heureDep;
        this.minDep = minDep;
        this.dureeVol = dureeVol;
    }
    
    /**
     * permet de rechercher un Aeroport avec son id
     * @param id String l'id d'un aeroport
     * @param Aero ArrayList<Aeroport> des aeroport ou chercher
     * @return l'aeroport voulue
     */
    public static Aeroport recherche(String id, ArrayList<Aeroport> Aero){
        for (int i = 0; i < Aero.size(); i++) {
            if (id.equals(Aero.get(i).getId())) {
                return Aero.get(i);
            }
        }
        return null;
    }
    
    /**
     * returne la a de l'aquation de droite de la forme ax+b
     * @param Aero ArrayList<Aeroport>
     * @return la a de l'aquation de droite de la forme ax+b
     */
    public double a(ArrayList<Aeroport> Aero){
        double x0,x1,y0,y1;
        x0=recherche(idAeroportDep, Aero).getX();
        y0=recherche(idAeroportDep, Aero).getY();
        
        x1=recherche(idAeroportArr, Aero).getX();
        y1=recherche(idAeroportArr, Aero).getY();
        return (y1-y0)/(x1-x0);
    }
    
    /**
     * returne la b de l'aquation de droite de la forme ax+b
     * @param Aero ArrayList<Aeroport>
     * @return la b de l'aquation de droite de la forme ax+b
     */
    public double b(ArrayList<Aeroport> Aero){
        double x0,x1,y0,y1;
        x0=recherche(idAeroportDep, Aero).getX();
        y0=recherche(idAeroportDep, Aero).getY();
        
        x1=recherche(idAeroportArr, Aero).getX();
        y1=recherche(idAeroportArr, Aero).getY();
        return y0-a(Aero)*x0;
    }

    public String getId() {
        return id;
    }

    public String getIdAeroportDep() {
        return idAeroportDep;
    }

    public String getIdAeroportArr() {
        return idAeroportArr;
    }

    public int getHeureDep() {
        return heureDep;
    }

    public int getMinDep() {
        return minDep;
    }

    public int getDureeVol() {
        return dureeVol;
    }
    
    
    
    @Override
    public String toString() {
        return "Vol{" + "id=" + id + ", idAeroportDep=" + idAeroportDep + ", idAeroportArr=" + idAeroportArr + ", heureDep=" + heureDep + ", minDep=" + minDep + ", dureeVol=" + dureeVol + '}';
    }
    
    
}
