/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15.modelisation;

import Groupes.Groupe15.Groupe15;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import static packMesClassesEtInterfaces.SAE202_Algos.calcul_caracteristiques;

/**
 *
 * @author Maxen
 */
public class Modelisation {
    Graph grapheCourant = new SingleGraph("");
    ArrayList<Aeroport> listeFranceAeroport ;
    ArrayList<Vol> listVol;
    
    /**
     * creer le graph
     * @param j numero du graph
     * @param prefixe nom du fichier du graph
     * @param millisecondes temp max pour fair l'algo
     */
    public Modelisation(int j,String prefixe, Long millisecondes) {
        try {
            FranceAeroport tmp = new FranceAeroport();
            listeFranceAeroport = tmp.getAeropoertListe();
        } catch (FileNotFoundException ex) {
            ;
        }
        try {
            ListeVol tmp= new ListeVol(prefixe, j);
            listVol=tmp.getListeVol();
        } catch (FileNotFoundException ex) {
            ;
        }
        if (System.currentTimeMillis()-Groupe15.currentTimeMillis>millisecondes-500) {
            return;
        }
        mettrePoint(millisecondes);
        
    }
    /**
     * met dans le graph les aretes si croisement avec moins de 15 minutes de différence
     * @param millisecondes temp max pour l'algo
     */
    private void creationArete(Long millisecondes){
        int tp=0;
        for (int i = 0; i < listVol.size(); i++) {
            
            if (System.currentTimeMillis()-Groupe15.currentTimeMillis>millisecondes-500) {
                return;
            }
            
            double m1 = listVol.get(i).a(listeFranceAeroport);
            double b1 = listVol.get(i).b(listeFranceAeroport);
            double x11=Vol.recherche(listVol.get(i).getIdAeroportArr(), listeFranceAeroport).getX();
            double x12=Vol.recherche(listVol.get(i).getIdAeroportDep(), listeFranceAeroport).getX();
            double y11=Vol.recherche(listVol.get(i).getIdAeroportArr(), listeFranceAeroport).getY();
            double y12=Vol.recherche(listVol.get(i).getIdAeroportDep(), listeFranceAeroport).getY();
            double d1=Math.sqrt((x11-x12)*(x11-x12)+(y11-y12)*(y11-y12));
            double v1= d1/listVol.get(i).getDureeVol(); // km par minute;
            
            
            for (int j = 0; j < listVol.size(); j++) {
                if (i!=j) {
                    double m2 = listVol.get(j).a(listeFranceAeroport);
                    double b2 = listVol.get(j).b(listeFranceAeroport);
                    // if les deux droite sont parallele il faut regarder si il n'y as pas de d'aller retoure qui pose probleme
                    if (m1 == m2) {
                        if (Vol.recherche(listVol.get(i).getIdAeroportDep(), listeFranceAeroport).equals(Vol.recherche(listVol.get(j).getIdAeroportArr(), listeFranceAeroport))||
                            Vol.recherche(listVol.get(j).getIdAeroportDep(), listeFranceAeroport).equals(Vol.recherche(listVol.get(i).getIdAeroportArr(), listeFranceAeroport))) {
                            // AB-CA
                            Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                            Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                            vol2.addmin(listVol.get(j).getDureeVol());
                            if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                                ajoutEdge(i, j);
                            }
                            
                            // AB-BC
                            vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                            vol1.addmin(listVol.get(i).getDureeVol());
                            vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                            if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                                ajoutEdge(i, j);
                            }
                        }
                        
                        break;
                    }
                    double x = (b2 - b1) / (m1 - m2);
                    double y = m1 * x + b1;
                    
                    double x21=Vol.recherche(listVol.get(j).getIdAeroportArr(), listeFranceAeroport).getX();
                    double x22=Vol.recherche(listVol.get(j).getIdAeroportDep(), listeFranceAeroport).getX();
                    double y21=Vol.recherche(listVol.get(j).getIdAeroportArr(), listeFranceAeroport).getY();
                    double y22=Vol.recherche(listVol.get(j).getIdAeroportDep(), listeFranceAeroport).getY();
                    
                    double d2=Math.sqrt((x21-x22)*(x21-x22)+(y21-y22)*(y21-y22));
                    double v2= d2/listVol.get(j).getDureeVol(); // km par minute;
                    
//                    System.out.println(+x12+"<="+x+"<="+x11);
                    // x doit etre compris entre Math.min(x12,x11) et Math.max(x12,x11)
                    if (Math.min(x12,x11)<x&& x<Math.max(x12,x11)) {
                        double distanceDepAx1 = Math.sqrt((x-x12)*(x-x12)+(y-y12)*(y-y12));
                        double distanceDepAx2 = Math.sqrt((x-x22)*(x-x22)+(y-y22)*(y-y22));
                        Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                        vol1.addmin((int) (distanceDepAx1/v1));
                        Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                        vol2.addmin((int) (distanceDepAx2/v2));
                        if (Math.abs(vol2.retourneEnMinute()-vol1.retourneEnMinute())<15) {
                           ajoutEdge(i, j);
                        }
                        
                    }
                    
                    /* regarde si des trajets de type AB-BC (vol1= départ A arrivée B, vol2=départ B arrivée C)Ces trajets se traitent comme les autres. 
                    Le point d’intersection est B. Il reste à vérifier si le critère d’horaire crée ou non un conflit*/
                    // AB-CA
                    if (Vol.recherche(listVol.get(i).getIdAeroportDep(), listeFranceAeroport).equals(Vol.recherche(listVol.get(j).getIdAeroportArr(), listeFranceAeroport))) {
                            Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                            Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                            vol2.addmin(listVol.get(j).getDureeVol());
                            if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                                ajoutEdge(i, j);
                            }
                        }
                    
                    // AB-BC
                    if (Vol.recherche(listVol.get(j).getIdAeroportDep(), listeFranceAeroport).equals(Vol.recherche(listVol.get(i).getIdAeroportArr(), listeFranceAeroport))) {
                        Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                        vol1.addmin(listVol.get(i).getDureeVol());
                        Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                        if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                            ajoutEdge(i, j);
                        }
                    }
                    // AB-CB
                    if (Vol.recherche(listVol.get(i).getIdAeroportArr(), listeFranceAeroport).equals(Vol.recherche(listVol.get(j).getIdAeroportArr(), listeFranceAeroport))) {
                        Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                        vol1.addmin(listVol.get(i).getDureeVol());
                        Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                        vol2.addmin(listVol.get(j).getDureeVol());
                        if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                            ajoutEdge(i, j);
                        }
                    }
                    // AB-AC
                    if (Vol.recherche(listVol.get(i).getIdAeroportDep(), listeFranceAeroport).equals(Vol.recherche(listVol.get(j).getIdAeroportDep(), listeFranceAeroport))) {
                        Heure vol1= new Heure(listVol.get(i).getHeureDep(), listVol.get(i).getMinDep());
                        Heure vol2= new Heure(listVol.get(j).getHeureDep(), listVol.get(j).getMinDep());
                        if (Math.abs(vol1.retourneEnMinute()-vol2.retourneEnMinute())<15) {
                            ajoutEdge(i, j);
                        }
                    }
                    
                    
                    //System.out.println(x +" "+ y);
                }
            }
            
        }
    }
    
    /**
     * ajout une arete a une entre 2 point
     * @param i int numero du point 1
     * @param j int numero du point 2
     */
    private void ajoutEdge(int i, int j){
        boolean tmp=true;
        for (Edge e: grapheCourant.getEachEdge()) {

            if (e.getNode0().getId().equals(listVol.get(i).getId())&&e.getNode1().getId().equals(listVol.get(j).getId())||e.getNode1().getId().equals(listVol.get(i).getId())&&e.getNode0().getId().equals(listVol.get(j).getId())) {
                tmp=false;
            }
        }
        if (tmp) {
            grapheCourant.addEdge(listVol.get(i).getId()+"_"+listVol.get(j).getId(), listVol.get(i).getId(), listVol.get(j).getId());
        }
    }
    
    /**
     * ca met les point dans le graph
     * @param millisecondes 
     */
    private void mettrePoint(Long millisecondes){
        for (int i = 0; i < listVol.size(); i++) {
            grapheCourant.addNode(listVol.get(i).getId());
        }
        
        creationArete(millisecondes);
        System.out.println(calcul_caracteristiques(grapheCourant));
    }

    /**
     * permet de retouner le graph
     * @return le graph
     */
    public Graph getGrapheCourant() {
        return grapheCourant;
    }
    
    
    
    @Override
    public String toString() {
        String tmp="";
        for (int i = 0; i < listeFranceAeroport.size(); i++) {
            tmp+=listeFranceAeroport.get(i);
            tmp+='\n';
        }
        return tmp;
    }
    
    
}
