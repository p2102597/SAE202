/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15.modelisation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Maxen
 */
public class ListeVol {
    private ArrayList<Vol> listeVol = new ArrayList<>();
    /**
     * initalisation des vol
     * @param i quelle liste a initaliser
     * @throws FileNotFoundException 
     */
    public ListeVol(String name,int j) throws FileNotFoundException {
        Scanner obj = new Scanner(new File("Donnees\\Test-model\\"+name+""+j+".csv"));
        
        while (obj.hasNextLine()) {
            String id=""; String idAeroportDep=""; String idAeroportArr=""; int heureDep=0; int minDep=0; int dureeVol=0;
            String Ligne= obj.nextLine();
            String tmp="";
            int y=0;
            for (int i = 0; i < Ligne.length(); i++) {
                if (Ligne.charAt(i)==';') {
                    if (y==0) {
                        id=tmp;
                    }
                    else if(y==1){
                        idAeroportDep=tmp;
                    }
                    else if(y==2){
                        idAeroportArr=tmp;
                    }
                    else if(y==3){
                        heureDep=Integer.parseInt(tmp);
                    }
                    else if(y==4){
                        minDep=Integer.parseInt(tmp);
                    }
                    tmp="";
                    i++;
                    y++;
                }
                tmp+=Ligne.charAt(i);
                if (i==Ligne.length()-1) {
                    dureeVol=Integer.parseInt(tmp);
                }
                
            }
            
            
            
            listeVol.add(new Vol(id, idAeroportDep, idAeroportArr, heureDep, minDep, dureeVol));
        }
        obj.close();
        
    }

    public ArrayList<Vol> getListeVol() {
        return listeVol;
    }
    
    
    
    @Override
    public String toString() {
        String tmp="";
        for (Vol a:listeVol) {
            tmp+=a;
            tmp+='\n';
        }
        return tmp;
    }
    
    
}
