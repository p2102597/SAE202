package Groupes.Groupe15;

/**
 *
 * @author educe
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.swingViewer.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//import org.graphstream.ui.view.Camera;
//import org.graphstream.ui.view.View;
//import org.graphstream.ui.view.Viewer;

/**
 *
 * @author Eric
 */
public class FenetrePrincipale extends JFrame {

    JPanel panneauDessin;
    PanneauChoixGraphe panneauChoixG;
    PanneauAlgo panneauAlgo;
    Graph grapheCourant = null;
    Graph listeGraphe[];

    private Viewer graphViewer;
    private View graphView;

    public FenetrePrincipale(Graph liste[], int nbFichiers) {
        listeGraphe = new Graph[nbFichiers];
        for (int i = 0; i < nbFichiers; i++) {
            listeGraphe[i]=liste[i];
        }
        panneauDessin = new JPanel();
        panneauChoixG = new PanneauChoixGraphe();
        panneauAlgo = new PanneauAlgo();
        panneauDessin.setLayout(new BorderLayout());
        panneauDessin.setPreferredSize(new Dimension(800, 600));
        this.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = gc.gridy = 0;
        gc.gridheight = 3;
        gc.fill = GridBagConstraints.HORIZONTAL;
        this.getContentPane().add(panneauDessin, gc);
        gc.gridx = 1;
        gc.gridheight = 1;
        this.getContentPane().add(panneauChoixG, gc);
        gc.gridy = 1;
        this.getContentPane().add(panneauAlgo, gc);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.pack();
    }

    public void updateDessin() {

        if (grapheCourant != null) {
            for(Node i: grapheCourant.getEachNode()){
                //i.setAttribute("ui.style", "fill-color: rgb(0,0,200); size: 20px, 20px;");
            }
            
            
            try {
                panneauDessin.remove((Component) graphViewer.getView(Viewer.DEFAULT_VIEW_ID));
            } catch (Exception ex) {
            }
            graphViewer = new Viewer(grapheCourant, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
            graphViewer.enableAutoLayout();
            graphView = graphViewer.addDefaultView(false);
            panneauDessin.add((Component) graphView);
            graphView.revalidate();
        }

    }

    class PanneauChoixGraphe extends JPanel {

        JComboBox<Graph> combo;
        JButton generer;
        JTextField jtf1 = new JTextField(15);
        JTextField jtf2 = new JTextField(15);
        JLabel dim1 = new JLabel("Dimension 1");
        JLabel dim2 = new JLabel("Dimension 2");

        public PanneauChoixGraphe() {
            this.setLayout(new GridLayout(3, 2));

            generer = new JButton("Generer");
            combo = new JComboBox();
            for (int i = 0; i < 20; i++) {
                combo.addItem(listeGraphe[i]);
            }

            this.setBorder(BorderFactory.createTitledBorder("Choix du graphe"));
            this.add(combo);
            this.add(generer);
            this.add(dim1);
            this.add(jtf1);
            this.add(dim2);
            this.add(jtf2);

            generer.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        grapheCourant = (Graph)combo.getSelectedItem();
                        updateDessin();
                    } catch (Exception exc) {
                    }
                }

            });

        }

    }

    class PanneauAlgo extends JPanel {

        JButton distance;
        JButton dominant;
        JLabel dominantl;
        JButton glouton;
        JLabel gloutonl;
        JButton astar;
        JTextField astarf;

        public PanneauAlgo() {
            this.setBorder(BorderFactory.createTitledBorder("Algorithmes"));
            this.setLayout(new GridBagLayout());
            GridBagConstraints gc = new GridBagConstraints();
            distance = new JButton("Distances");
            dominant = new JButton("Dominant");
            dominantl = new JLabel("");
            glouton = new JButton("Coloration");
            gloutonl = new JLabel("");
            gc.fill = GridBagConstraints.HORIZONTAL;
            gc.gridx = gc.gridy = 0;
            gc.weightx = .8;
            gc.insets = new Insets(5, 5, 5, 5);
            this.add(distance, gc);
            gc.gridy = 1;
            this.add(dominant, gc);
            gc.gridx = 1;
            gc.weightx = .2;
            this.add(dominantl, gc);
            gc.gridy = 2;
            gc.gridx = 0;
            gc.weightx = .8;
            this.add(glouton, gc);
            gc.gridx = 1;
            gc.weightx = .2;
            this.add(gloutonl, gc);

            distance.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    updateDessin();
                }

                
            });

            dominant.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    updateDessin();
                }

                
            });

            glouton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    FenetrePrincipale.colorierGraphe(grapheCourant, "couleur");
                    updateDessin();
                }

            });
        }
    }
    public static void colorierGraphe(Graph g, String attribut) {
        int max = g.getNodeCount();
        Color[] cols = new Color[max + 1];
        for (int i = 0; i <= max; i++) {
            cols[i] = Color.getHSBColor((float) (Math.random()), 0.8f, 0.9f);
        }

        for (Node n : g) {
            int col = n.getAttribute(attribut);
            if (n.hasAttribute("ui.style")) {
                n.setAttribute("ui.style", "fill-color:rgba(" + cols[col].getRed() + "," + cols[col].getGreen() + "," + cols[col].getBlue() + ",200);");
            } else {
                n.addAttribute("ui.style", "fill-color:rgba(" + cols[col].getRed() + "," + cols[col].getGreen() + "," + cols[col].getBlue() + ",200);");
            }
        }
    }
}
