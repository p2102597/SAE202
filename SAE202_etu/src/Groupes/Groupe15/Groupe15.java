/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Groupes.Groupe15;

import Groupes.Groupe15.modelisation.Modelisation;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import packMesClassesEtInterfaces.SAE202_Algos;
import static packMesClassesEtInterfaces.SAE202_Algos.calcul_caracteristiques;
import static packMesClassesEtInterfaces.SAE202_Algos.compte_nb_conflits;
import packMesClassesEtInterfaces.SAE202_Interface;

/**
 *
 *
 */
public class Groupe15 implements SAE202_Interface {

    public static Long temps;
    public static Long temps2;
    public static Long currentTimeMillis;
    int tmp;
    int o;
    boolean atrouve = false;
    public static Long tempsGlobal;

    @Override
    public void challenge(String prefixeFichier, Integer nbFichiers, Long millisecondes) {
        tempsGlobal = System.currentTimeMillis();
        Graph listeGraphe[] = new Graph[nbFichiers];

        for (int i = 0; i < nbFichiers; i++) {
            listeGraphe[i] = SAE202_Algos.charger_graphe(prefixeFichier + i + ".txt");

        }

        for (int i = 0; i < nbFichiers; i++) {
            System.out.println("graphe " + i + " : " + this.colorier(listeGraphe[i], tempsGlobal));
            tempsGlobal = temps;
        }
        /*
        listeConflitMin[i] = 0;

Integer listeConflitMin[] = new Integer[nbFichiers];

temps = (millisecondes - (System.currentTimeMillis() - temps)) / 21;
        temps2 = System.currentTimeMillis();
//        System.out.println(temps);
        for(int i = 0; i < nbFichiers; i++){
            o = 0;
            atrouve = false;

            while (System.currentTimeMillis() - temps2 < temps * (i+1) && atrouve != true){
                AlgoDescente.Descente(listeGraphe[i], listeKmax[i]);
                tmp = compte_nb_conflits(listeGraphe[i], "couleurD");
                 if (i == 3){
//                     System.out.println("stop val " + tmp);
                 }

                if (o == 0){
                    o = 1;
                    listeConflitMin[i] = tmp;
                }

                if (listeConflitMin[i] > tmp){
                    listeConflitMin[i] = tmp;
                    System.out.println(" graphe " + i + " " +listeConflitMin[i]);


                    }
                }
                if (listeConflitMin[i] == 0){
                    atrouve = true;
                    System.out.println("i = " + i);
                }
            }

        for (int i = 0; i < nbFichiers; i ++){
            System.out.println("graphe " + i + " min conflit = " + compte_nb_conflits(listeGraphe[i],"couleur"));
            if (compte_nb_conflits(listeGraphe[i], "couleurD") < compte_nb_conflits(listeGraphe[i], "couleur")){
                for (int z = 0; z < listeGraphe[i].getNodeCount(); z ++){

                listeGraphe[i].getNode(z).setAttribute("couleur", (int)listeGraphe[i].getNode(z).getAttribute("couleurD"));
            }
        }
        }

        
        
         */

        try {
            FileOutputStream sauve = new FileOutputStream("Resultats\\coloration-groupe15.txt");
            for (int i = 0; i < nbFichiers; i++) {
                sauve.write((listeGraphe[i].getId() + ";" + SAE202_Algos.compte_nb_conflits(listeGraphe[i], "couleur") + "\n").getBytes(StandardCharsets.UTF_8));
                SAE202_Algos.sauver_coloration(listeGraphe[i], "couleur", 15);
            }
        } catch (Exception ex) {
            System.out.println("échec de la sauvegarde");
        }

        /*FenetrePrincipale fp = new FenetrePrincipale(listeGraphe,nbFichiers);
        fp.setVisible(true);*/
    }

    @Override
    public void modelisation(String prefixeFichier, Integer nbFichiers, Long millisecondes) {
        currentTimeMillis = System.currentTimeMillis();

        try {
            String encoding = "UTF-8";
            PrintWriter writer;
            writer = new PrintWriter("Resultats\\modelisation-groupe15.csv", encoding);
            writer.println("Fichier;nbNoeuds;nbAretes;degreMoyen;nbComposantes;diametre");
            for (int i = 0; i < nbFichiers; i++) {
                Modelisation test = new Modelisation(i, prefixeFichier, millisecondes);
                Graph grapheCourant = test.getGrapheCourant();
                HashMap<String, Object> cara = calcul_caracteristiques(grapheCourant);

                writer.print(prefixeFichier + i + ".csv;");

                writer.print(cara.get("nbNoeuds") + ";");
                writer.print(cara.get("nbAretes") + ";");
                writer.print(cara.get("degreMoyen") + ";");
                writer.print(cara.get("nbComposantes") + ";");
                writer.print(cara.get("diametre") + ";");

                writer.println();
            }

            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Modelisation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Modelisation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int colorier(Graph g, Long millisecondes) {
        temps = System.currentTimeMillis();
        int kmax = g.getAttribute("nb_couleurs_max");
        int conflit = AlgoDSAT.run(g, kmax);
        if (AlgoWelshPowel.run(g, kmax) > conflit) {
            for (Node n : g.getEachNode()) {
                n.addAttribute("couleur_opt", -1);
                n.setAttribute("couleur_opt", (int) n.getAttribute("couleurDSAT"));
            }
        } else {
            for (Node n : g.getEachNode()) {
                n.addAttribute("couleur_opt", -1);
                n.setAttribute("couleur_opt", (int) n.getAttribute("couleur"));
            }
        }

        return SAE202_Algos.compte_nb_conflits(g, "couleur_opt");
    }
}
