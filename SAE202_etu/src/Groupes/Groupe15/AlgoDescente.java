/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Groupes.Groupe15;

import java.util.ArrayList;
import java.util.Iterator;
import org.graphstream.graph.Graph;
import static packMesClassesEtInterfaces.SAE202_Algos.compte_nb_conflits;

/**
 *
 * @author alexg
 */
public class AlgoDescente {
    
    

    public AlgoDescente() {
        System.out.println("constructeur");
    }

    public static Graph Descente(Graph g, int Kmax) {
        
        int a = -1;
        
        AlgoDSAT.run(g, Kmax);


        ArrayList Erreurnode0 = new ArrayList();

        boolean fin = false;

        for (int i = 0; i < g.getNodeCount(); i ++){
            g.getNode(i).addAttribute("TD", a);
            g.getNode(i).addAttribute("SMinD", a);
            g.getNode(i).addAttribute("couleurD", a);
            g.getNode(i).setAttribute("SMinD", (int)g.getNode(i).getAttribute("couleur"));
            g.getNode(i).setAttribute("couleurD", (int)g.getNode(i).getAttribute("couleur"));
        }
        


        while (fin == false) {
            
            Erreurnode0.clear();
            
            for (int i = 0; i < g.getNodeCount(); i ++){
                g.getNode(i).setAttribute("TD", (int)g.getNode(i).getAttribute("couleurD"));
                
            }

            

            for (int i = 0; i < g.getEdgeCount(); i++) {

                if (g.getEdge(i).getNode0().getAttribute("couleurD") == g.getEdge(i).getNode1().getAttribute("couleurD")) {
                    if (Erreurnode0.contains(g.getEdge(i).getNode0().getIndex()) != true) {
                        Erreurnode0.add(g.getEdge(i).getNode0().getIndex());
                    }
                    if (Erreurnode0.contains(g.getEdge(i).getNode1().getIndex()) != true) {
                        Erreurnode0.add(g.getEdge(i).getNode1().getIndex());
                    }
                }
            }

            for (int i = 0; i < Erreurnode0.size(); i++) {
                for (int x = 0; x < Kmax - 1; x++) {
                    for (int z = 0; z < g.getNodeCount(); z ++){
                        g.getNode(z).setAttribute("TD", (int)g.getNode(i).getAttribute("couleurD"));
                    }
                    g.getNode((Integer) Erreurnode0.get(i)).setAttribute("TD", x);
                    if (compte_nb_conflits(g, "TD") < compte_nb_conflits(g, "SMinD")) {
                        for (int z = 0; z < g.getNodeCount(); z ++){
                            g.getNode(z).setAttribute("SMinD", (int)g.getNode(z).getAttribute("TD"));
                        }
                    }
                }
            }

            if (compte_nb_conflits(g, "SMinD") == compte_nb_conflits(g, "couleurD")) {

                fin = true;
            }

            for (int z = 0; z < g.getNodeCount(); z ++){
                g.getNode(z).setAttribute("couleurD", (int)g.getNode(z).getAttribute("SMinD"));
            }

        }
        
        return g;

    }
    
}
