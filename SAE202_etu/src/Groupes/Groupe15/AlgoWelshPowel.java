/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15;

import org.graphstream.algorithm.coloring.WelshPowell;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import packMesClassesEtInterfaces.SAE202_Algos;

/**
 * Classe statique qui permet d'exécuter un algorythme de Welsh Powel
 * @author hmill
 */
public class AlgoWelshPowel {
    /**
     * Algorythme de Welsh Powel appliqué à un seul graphe
     * @param g le graphe sur lequel l'algorythme porte
     * @param kmax le nombre de couleur max
     * @return le nombre de conflit générer
     */
    public static int run(Graph g, int kmax){
        WelshPowell wp =new WelshPowell("couleur");
        wp.init(g);
        wp.compute();
        if(wp.getChromaticNumber()>kmax){
            for(Node n : g.getEachNode()){
                if((int)n.getAttribute("couleur") > kmax){
                    n.setAttribute("couleur", (int)n.getAttribute("couleur")%kmax);
                }
            }
        }
        return (Integer)SAE202_Algos.compte_nb_conflits(g, "couleur");
    }
}
