/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15;

import org.graphstream.graph.*;
import packMesClassesEtInterfaces.SAE202_Algos;

/**
 * Classe permettant d'appeler la méthode static qui colorie un graphe aléatoirement
 * @author hmill
 */
public class AlgoAlea {
    /**
     * Fonction qui permet de colorie un graphe aléatoirement
     * @param g le graphe à colorier
     * @param kmax le nombre maximum de couleur voulu
     * @return retourne le nombre de conflit générer après la coloration
     */
    public static int run(Graph g, int kmax){
        for(Node n : g.getEachNode()){
            n.addAttribute("couleur", (int)(Math.random()*kmax));
        }
        return SAE202_Algos.compte_nb_conflits(g, "couleur");
    }
}
