/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15;

import java.util.Iterator;
import org.graphstream.algorithm.coloring.WelshPowell;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import packMesClassesEtInterfaces.DSat;
import packMesClassesEtInterfaces.SAE202_Algos;

/**
 * Classe statique qui exécute l'algorythme de DSAT
 * @author hmill
 */
public class AlgoDSAT {
    /**
     * Algorythme de DSAT appliqué à un seul graphe
     * @param g le graphe sur lequel l'algorythme porte
     * @param kmax le nombre de couleur max
     * @return le nombre de conflit générer
     */
    public static int run(Graph g, int kmax){
        DSat dsat =new DSat("couleurDSAT");
        Integer listeCouleur[]=new Integer[kmax];
        dsat.init(g);
        dsat.compute();
        for(int i = 0; i < kmax; i++){
            listeCouleur[i]=0;
        }
        if(dsat.getChromaticNumber()>kmax){
            for(Node n : g.getEachNode()){
                if((int)n.getAttribute("couleurDSAT") >= kmax){
                    n.setAttribute("couleurDSAT", (int)n.getAttribute("couleurDSAT")%kmax);
                    Iterator it = n.getNeighborNodeIterator();
                    while(it.hasNext()){
                        Node voisin = (Node)it.next();
                        if((int)voisin.getAttribute("couleurDSAT") < kmax){
                            listeCouleur[(int)voisin.getAttribute("couleurDSAT")]++;
                        }
                    }
                    int min = 0;
                    for(int i = 1; i < kmax; i++){
                        if(listeCouleur[i] < listeCouleur[min]){
                            min = i;
                        }
                    }
                    n.setAttribute("couleurDSAT", min);
                }
            }
        }
        return (Integer)SAE202_Algos.compte_nb_conflits(g, "couleurDSAT");
    }
}