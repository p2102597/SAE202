/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Groupes.Groupe15;

import java.util.ArrayList;
import java.util.Iterator;
import jdk.internal.net.http.common.Pair;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import packMesClassesEtInterfaces.SAE202_Algos;

/**
 *
 * @author hugom
 */
public class AlgoTabou {

    public void run(Graph g, int kmax, int tailleFIFO, int nbIteration) {
        ArrayList<String> fifo = new ArrayList<>();
        String resultat;
        for (int i = 0;; i++) {
            resultat = this.calculVoisin(g, fifo);
            if (resultat == null) {
                return;
            } else {
                fifo.add(resultat);
                if (fifo.size() > tailleFIFO) {
                    fifo.remove(0);
                }
            }
        }
    }

    private String calculVoisin(Graph g, ArrayList<String> fifo) {
        int nbErreurOrigine = SAE202_Algos.compte_nb_conflits(g, "couleur");
        ArrayList<Integer> solution1 = new ArrayList<>();
        ArrayList<Integer> solution2 = new ArrayList<>();
        for (Edge e : g.getEachEdge()) {
            if (e.getNode0().getAttribute("couleur") == e.getNode1().getAttribute("couleur")) {
                solution1.clear();
                solution1 = this.calculCouleurDispo(e.getNode0());
                e.getNode0().addAttribute("couleurTabouTemp", (int)e.getNode0().getAttribute("couleur"));
                e.getNode0().setAttribute("couleur", solution1.get(0));
                Integer min1 = 0;
                Integer meilleurSol1 = SAE202_Algos.compte_nb_conflits(g, "couleur");
                for (int i = 1; i < solution1.size(); i++) {
                    e.getNode0().setAttribute("couleur", solution1.get(i));
                    if (SAE202_Algos.compte_nb_conflits(g, "couleur") <= meilleurSol1) {
                        min1 = i;
                        meilleurSol1 = SAE202_Algos.compte_nb_conflits(g, "couleur");
                    }
                }
                e.getNode0().setAttribute("couleur", e.getNode0().getAttribute("couleurTambouTemp"));
                ///////////////////////////////////////
                solution2.clear();
                solution2 = this.calculCouleurDispo(e.getNode1());
                e.getNode1().addAttribute("couleurTabouTemp", e.getNode1().getAttribute("couleur"));
                e.getNode1().setAttribute("couleur", solution2.get(0));
                Integer min2 = 0;
                Integer meilleurSol2 = SAE202_Algos.compte_nb_conflits(g, "couleur");
                for (int i = 1; i < solution2.size(); i++) {
                    e.getNode1().setAttribute("couleur", solution2.get(i));
                    if (SAE202_Algos.compte_nb_conflits(g, "couleur") <= meilleurSol2) {
                        min2 = i;
                        meilleurSol2 = SAE202_Algos.compte_nb_conflits(g, "couleur");
                    }
                }
                e.getNode1().setAttribute("couleur", e.getNode1().getAttribute("couleurTambouTemp"));
                if (meilleurSol1 < meilleurSol2) {
                    for (int i = 0; i < fifo.size(); i++) {
                        if (!fifo.get(i).equals(e.getNode0().getId())) {
                            e.getNode0().setAttribute("couleur", solution1.get(min1));
                            return e.getNode0().getId();
                        }
                    }

                } else if (meilleurSol1 > meilleurSol2) {
                    for (int i = 0; i < fifo.size(); i++) {
                        if (!fifo.get(i).equals(e.getNode1().getId())) {
                            e.getNode1().setAttribute("couleur", solution2.get(min2));
                            return e.getNode1().getId();
                        }
                    }
                }
            }
        }
        return null;
    }

    private ArrayList<Integer> calculCouleurDispo(Node n) {
        ArrayList<Integer> listeCouleur = new ArrayList<>();
        Iterator it = n.getNeighborNodeIterator();
        int i = 0;
        listeCouleur.clear();
        while (it.hasNext()) {
            System.out.println("1:" + i++);
            listeCouleur.add(0);
            it.next();
        }
        it = n.getNeighborNodeIterator();
        while (it.hasNext()) {
            Node voisin = (Node) it.next();
            listeCouleur.set(voisin.getAttribute("couleur"), (int) (listeCouleur.get(voisin.getAttribute("couleur")) + 1));
        }
        int min = 0;
        for (int j = 1; j < listeCouleur.size(); j++) {
            if (min > listeCouleur.get(j)) {
                min = j;
            }
        }
        ArrayList<Integer> solution = new ArrayList<>();
        for (int j = 0; j < listeCouleur.size(); j++) {
            if (listeCouleur.get(j) == min) {
                solution.add(j);
            }
        }
        return solution;
    }
}
